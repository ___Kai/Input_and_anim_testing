using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dontlagmylaptop : MonoBehaviour
{
    private float maxLifeTime = 20f;
    private float livedTime;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        livedTime += Time.deltaTime;
        if(livedTime >= maxLifeTime)
        {
            Destroy(gameObject);
        }
    }
}

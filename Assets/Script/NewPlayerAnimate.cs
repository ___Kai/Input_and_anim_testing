using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewPlayerAnimate : MonoBehaviour
{
    [SerializeField]
    private Animator anim;
    private Rigidbody rb;
    private float maxSpeed = 1f;
    // Start is called before the first frame update
    void Start()
    {
        //anim = this.GetComponent<Animator>();
        rb = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetFloat("Speed", rb.velocity.magnitude / maxSpeed);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicBoxLogic : MonoBehaviour
{
    //[SerializeField]
    //private GameObject effect;
    [SerializeField]
    private Rigidbody playerRB;
    [SerializeField]
    private float force = 10f;
    [SerializeField]
    private float radius = 3f;
    private void OnCollisionEnter(Collision collision)
    {
        playerRB.AddExplosionForce(force, transform.position, radius);
        Debug.Log("What happened to the kaboom? There was supposed to be an earth-shattering kaboom!");
        Destroy(gameObject);
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class NewPlayerMovement : MonoBehaviour
{
    private CharlieActions charliesActions;
    private InputAction direction;

    private Rigidbody charliesRB;
    [SerializeField]
    private float movementForce = 1f;
    [SerializeField]
    private float jumpForce = 5f;
    [SerializeField]
    private float maxSpeed = 5f;
    private Vector3 forceDirection = Vector3.zero;
    private int jumpNum = 0;

    [SerializeField]
    private Camera playerCamera;

    private void Awake()
    {
        charliesRB = this.GetComponent<Rigidbody>();
        charliesActions = new CharlieActions();
    }

    private void OnEnable()
    {
        charliesActions.Move.Jump.started += DoJump;
        direction = charliesActions.Move.Direction;
        charliesActions.Move.Enable();
    }

    private void DoJump(InputAction.CallbackContext obj)
    {
        if(isGrounded()|| jumpNum == 1)
        {
            forceDirection += Vector3.up * jumpForce;
            jumpNum += 1;
        }
    }

    private void OnDisable()
    {
        charliesActions.Move.Jump.started -= DoJump;
        charliesActions.Move.Disable();
    }

    private void FixedUpdate()
    {
        forceDirection += direction.ReadValue<Vector2>().x * GetCameraRight(playerCamera) * movementForce;
        forceDirection += direction.ReadValue<Vector2>().y * GetCameraForward(playerCamera) * movementForce;

        charliesRB.AddForce(forceDirection, ForceMode.Impulse);
        forceDirection = Vector3.zero;

        if(charliesRB.velocity.y < 0f)
        {
            charliesRB.velocity -= Vector3.down * Physics.gravity.y * Time.fixedDeltaTime * 1.75f;
        }
        Vector3 horVelocity = charliesRB.velocity;
        horVelocity.y = 0;
        if(horVelocity.sqrMagnitude > maxSpeed * maxSpeed)
        {
            charliesRB.velocity = horVelocity.normalized * maxSpeed + Vector3.up * charliesRB.velocity.y;
        }

        LookAt();
    }

    private void LookAt()
    {
        Vector3 dir = charliesRB.velocity;
        dir.y = 0;
        if(direction.ReadValue<Vector2>().sqrMagnitude > 0.1f && dir.sqrMagnitude > 0.1f)
        {
            this.charliesRB.rotation = Quaternion.LookRotation(dir, Vector3.up);
        }
        else
        {
            charliesRB.angularVelocity = Vector3.zero;
        }
    }

    private Vector3 GetCameraForward(Camera playerCamera)
    {
        Vector3 forward = playerCamera.transform.forward;
        forward.y = 0;
        return forward.normalized;
    }

    private Vector3 GetCameraRight(Camera playerCamera)
    {
        Vector3 right = playerCamera.transform.right;
        right.y = 0;
        return right.normalized;
    }

    private bool isGrounded()
    {
        Ray ray = new Ray(this.transform.position + Vector3.up * 0.35f, Vector3.down);
        if (Physics.Raycast(ray, out RaycastHit hit, 1f))
        {
            jumpNum = 0;
            return true;
        }
        else
        {
            return false;
        }
    }
}

using System;
using UnityEngine;

public class HazardSpawner : MonoBehaviour
{
    public GameObject hazardPrefab;
    public float timeBetweenWaves;
    public float countDown;
    private int waveIndex = 1;
    public Transform spawnLocation;
    //public Text waveCountdown;
    private void Update()
    {
        if (countDown <= 0f)
        {
            SpawnWave();
            countDown = timeBetweenWaves;
        }
        countDown -= Time.deltaTime;

        //waveCountdown.text = Mathf.Round(countDown).ToString();
    }
    void SpawnWave()
    {
        for (int i = 0; i < waveIndex; i++)
        {
            SpawnEnemy();
        }
        //waveIndex++;
    }
    void SpawnEnemy()
    {
        Instantiate(hazardPrefab, spawnLocation.position, spawnLocation.rotation);
    }
}

